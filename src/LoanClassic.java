import java.util.Scanner;

public class LoanClassic {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter the loan amount, UAH: ");
        double amount = in.nextDouble();

        System.out.print("Enter the annual interest rate, %: ");
        double rate = in.nextDouble();

        System.out.print("Enter the loan term, months: ");
        int term = in.nextInt();

        System.out.println();
        /*
        double monthlyDifferentiatedPaymentFirst;
        double monthlyDifferentiatedPaymentSecond;
        double monthlyDifferentiatedPaymentThird;
        double monthlyDifferentiatedPaymentFourth;
        double monthlyDifferentiatedPaymentFifth;
        double monthlyDifferentiatedPaymentSixth;
        double monthlyDifferentiatedPaymentSeventh;
        double monthlyDifferentiatedPaymentEighth;
        double monthlyDifferentiatedPaymentNinth;
        double monthlyDifferentiatedPaymentTenth;
        double monthlyDifferentiatedPaymentEleventh;
        double monthlyDifferentiatedPaymentTwelfth;
        */
        double monthlyLoanBody;

        double balanceLoanSecond;
        double balanceLoanThird;
        double balanceLoanFourth;
        double balanceLoanFifth;
        double balanceLoanSixth;
        double balanceLoanSeventh;
        double balanceLoanEighth;
        double balanceLoanNinth;
        double balanceLoanTenth;
        double balanceLoanEleventh;
        double balanceLoanTwelfth;

        double monthlyLoanInterestFirst;
        double monthlyLoanInterestSecond;
        double monthlyLoanInterestThird;
        double monthlyLoanInterestFourth;
        double monthlyLoanInterestFifth;
        double monthlyLoanInterestSixth;
        double monthlyLoanInterestSeventh;
        double monthlyLoanInterestEighth;
        double monthlyLoanInterestNinth;
        double monthlyLoanInterestTenth;
        double monthlyLoanInterestEleventh;
        double monthlyLoanInterestTwelfth;

        double totalInterest;

        // розрахунок суми, що йде на погашення тіла кредиту
        monthlyLoanBody = amount / term;
        System.out.println("The amount that goes to repay the body of the loan, UAH: " + monthlyLoanBody);
        System.out.println();

        // розрахунок залишку тіла кредиту, починаючи в другого місяця користування кредитом
        balanceLoanSecond = amount - monthlyLoanBody;
        balanceLoanThird = balanceLoanSecond - monthlyLoanBody;
        balanceLoanFourth = balanceLoanThird - monthlyLoanBody;
        balanceLoanFifth = balanceLoanFourth - monthlyLoanBody;
        balanceLoanSixth = balanceLoanFifth - monthlyLoanBody;
        balanceLoanSeventh = balanceLoanSixth - monthlyLoanBody;
        balanceLoanEighth = balanceLoanSeventh - monthlyLoanBody;
        balanceLoanNinth = balanceLoanEighth - monthlyLoanBody;
        balanceLoanTenth = balanceLoanNinth - monthlyLoanBody;
        balanceLoanEleventh = balanceLoanTenth - monthlyLoanBody;
        balanceLoanTwelfth = balanceLoanEleventh - monthlyLoanBody;
        /*
        System.out.println("The balance of the loan for the second billing period is, UAH: " + balanceLoanSecond);
        System.out.println("The balance of the loan for the third billing period is, UAH: " + balanceLoanThird);
        System.out.println("The balance of the loan for the fourth billing period is, UAH: " + balanceLoanFourth);
        System.out.println("The balance of the loan for the fifth billing period is, UAH: " + balanceLoanFifth);
        System.out.println("The balance of the loan for the sixth billing period is, UAH: " + balanceLoanSixth);
        System.out.println("The balance of the loan for the seventh billing period is, UAH: " + balanceLoanSeventh);
        System.out.println("The balance of the loan for the eighth billing period is, UAH: " + balanceLoanEighth);
        System.out.println("The balance of the loan for the ninth billing period is, UAH: " + balanceLoanNinth);
        System.out.println("The balance of the loan for the tenth billing period is, UAH: " + balanceLoanTenth);
        System.out.println("The balance of the loan for the eleventh billing period is, UAH: " + balanceLoanEleventh);
        System.out.println("The balance of the loan for the twelfth billing period is, UAH: " + balanceLoanTwelfth);
        System.out.println();
        */
        // розрахунок суми, що йде на погашення процентів по кредиту
        monthlyLoanInterestFirst = amount * rate / (term * 100);
        monthlyLoanInterestSecond = balanceLoanSecond * rate / (term * 100);
        monthlyLoanInterestThird = balanceLoanThird * rate / (term * 100);
        monthlyLoanInterestFourth = balanceLoanFourth * rate / (term * 100);
        monthlyLoanInterestFifth = balanceLoanFifth * rate / (term * 100);
        monthlyLoanInterestSixth = balanceLoanSixth * rate / (term * 100);
        monthlyLoanInterestSeventh = balanceLoanSeventh * rate / (term * 100);
        monthlyLoanInterestEighth = balanceLoanEighth * rate / (term * 100);
        monthlyLoanInterestNinth = balanceLoanNinth * rate / (term * 100);
        monthlyLoanInterestTenth = balanceLoanTenth * rate / (term * 100);
        monthlyLoanInterestEleventh = balanceLoanEleventh * rate / (term * 100);
        monthlyLoanInterestTwelfth = balanceLoanTwelfth * rate / (term * 100);

        System.out.println("The first billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestFirst);
        System.out.println("The second billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestSecond);
        System.out.println("The third billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestThird);
        System.out.println("The fourth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestFourth);
        System.out.println("The fifth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestFifth);
        System.out.println("The sixth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestSixth);
        System.out.println("The seventh billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestSeventh);
        System.out.println("The eighth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestEighth);
        System.out.println("The ninth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestNinth);
        System.out.println("The tenth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestTenth);
        System.out.println("The eleventh billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestEleventh);
        System.out.println("The twelfth billing period. The amount that goes to repay the interest of the loan is, UAH: " + monthlyLoanInterestTwelfth);
        System.out.println();

        /*
        // розрахунок диференційованого платежу
        monthlyDifferentiatedPaymentFirst = monthlyLoanBody + monthlyLoanInterestFirst;
        monthlyDifferentiatedPaymentSecond = monthlyLoanBody + monthlyLoanInterestSecond;
        monthlyDifferentiatedPaymentThird = monthlyLoanBody + monthlyLoanInterestThird;
        monthlyDifferentiatedPaymentFourth = monthlyLoanBody + monthlyLoanInterestFourth;
        monthlyDifferentiatedPaymentFifth = monthlyLoanBody + monthlyLoanInterestFifth;
        monthlyDifferentiatedPaymentSixth = monthlyLoanBody + monthlyLoanInterestSixth;
        monthlyDifferentiatedPaymentSeventh = monthlyLoanBody + monthlyLoanInterestSeventh;
        monthlyDifferentiatedPaymentEighth = monthlyLoanBody + monthlyLoanInterestEighth;
        monthlyDifferentiatedPaymentNinth = monthlyLoanBody + monthlyLoanInterestNinth;
        monthlyDifferentiatedPaymentTenth = monthlyLoanBody + monthlyLoanInterestTenth;
        monthlyDifferentiatedPaymentEleventh = monthlyLoanBody + monthlyLoanInterestEleventh;
        monthlyDifferentiatedPaymentTwelfth = monthlyLoanBody + monthlyLoanInterestTwelfth;

        System.out.println("The first billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentFirst);
        System.out.println("The second billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentSecond);
        System.out.println("The third billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentThird);
        System.out.println("The fourth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentFourth);
        System.out.println("The fifth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentFifth);
        System.out.println("The sixth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentSixth);
        System.out.println("The seventh billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentSeventh);
        System.out.println("The eighth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentEighth);
        System.out.println("The ninth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentNinth);
        System.out.println("The tenth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentTenth);
        System.out.println("The eleventh billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentEleventh);
        System.out.println("The twelfth billing period. The amount of differentiated payment on the loan is, UAH: " + monthlyDifferentiatedPaymentTwelfth);
        System.out.println();
        */
        // розрахунок переплати за 12 місяців користування кредитними коштами
        totalInterest = monthlyLoanInterestFirst + monthlyLoanInterestSecond + monthlyLoanInterestThird + monthlyLoanInterestFourth + monthlyLoanInterestFifth + monthlyLoanInterestSixth + monthlyLoanInterestSeventh + monthlyLoanInterestEighth + monthlyLoanInterestNinth + monthlyLoanInterestTenth + monthlyLoanInterestEleventh + monthlyLoanInterestTwelfth;
        System.out.println("The amount of overpayment for 12 months of using the loan, UAH: " + totalInterest);
    }
}
